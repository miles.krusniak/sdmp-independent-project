require 'csv'

# A class for static pages - home, about, etc.
class StaticController < ApplicationController
  def home # GET "/"
    # nothing to do
  end

  def import # POST "/" ?
    @string = []
    @file = CSV.read(params[:file].path)
    @file.shift
    @string = @file.collect { |row| [row[0], row.count('YES')] }
    @string = @string.max(5) { |a, b| a[1] - b[1] }.to_s
    render :analyze
  end
end