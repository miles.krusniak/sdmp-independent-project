Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static#home', as: :home
  post '/', to: 'static#import', as: :import
  get 'csv', to: 'static#analyze', as: :analyze
end