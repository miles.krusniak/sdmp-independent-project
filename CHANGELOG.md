
### 12/16 9:15 AM
Created the project, initialized Git things, got Rails working and gems installed. I figure I may as well work on master until the skeleton of the project is finished since it's just me here.

### 12/16 9:45 AM
Created a static controller for the home page. Now is probably a good time to think about models too. 

The project description we have is "your application should analyze the availability of participants and algorithmically determine the top 5 most optimal time slots, or if the proposed event is impossible given everyone’s schedule." To me that indicates that the only Model-able things there are CSV imports and users. But we don't really need to persist the CSV files, and by extension we don't need to persist the users either. (In fact When2Meet doesn't even really have a concept of users.)

I'm curious to see if we can make this using only static pages. I think the most technically challenging part here is going to be importing the CSV file, but again, we can totally have that data just hanging around without ever needing to write to a database. I think next I'll take a look into what the CSV import entails - there must be some Gems out there.